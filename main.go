package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/tarm/serial"
)

var portName string

const (
	celsius    = 0x63
	fahrenheit = 0x66
)

type Response struct {
	Temp   float64 `json:"temperature"`
	Unit   string  `json:"unit"`
	Time   string  `json:"time"`
	Status string  `json:"status"`
}

func readTemp(unit byte, attempts int) (float64, error) {
	c := &serial.Config{
		Name: portName,
		Baud: 115200,
	}
	p, err := serial.OpenPort(c)
	if err != nil {
		return -9999.0, err
	}
	defer p.Close()
	var temperature float64 = -9999.0
	for i := 0; i < attempts; i++ {
		if err = p.Flush(); err != nil {
			continue
		}
		if _, err = p.Write([]byte{unit}); err != nil {
			continue
		}
		rxed := make([]byte, 128)
		n, err := p.Read(rxed)
		if err != nil {
			continue
		}
		rxString := string(rxed[:n])
		temperature, err := strconv.ParseFloat(strings.TrimSpace(rxString), 64)
		if err != nil {
			continue
		}
		// probably an incorrect value- reading from serial occasionally gives
		// incomplete values
		if temperature < 10.0 {
			err = fmt.Errorf("invalid value: %f", temperature)
			continue
		}
	}
	return temperature, err
}

func main() {
	if len(os.Args) != 3 {
		log.Fatal("invalid args. usage:\nserial-server <port name> <http port>")
	}
	portName = os.Args[1]
	httpPort := os.Args[2]
	http.HandleFunc("/sensor/temp/", tempHandler)
	http.ListenAndServe(":"+httpPort, nil)
}

func tempHandler(w http.ResponseWriter, r *http.Request) {
	unit := r.URL.Path[len("/sensor/temp/"):]
	var temp float64
	var err error
	var unitString string
	switch unit {
	case "c":
		temp, err = readTemp(celsius, 3)
		unitString = "celsius"
		break
	case "f":
		temp, err = readTemp(fahrenheit, 3)
		unitString = "fahrenheit"
		break
	default:
		resp := &Response{
			Temp:   -9999.0,
			Unit:   "n/a",
			Time:   time.Now().Format("Jan 2 03:04 PM MST 2006"),
			Status: fmt.Sprintf("invalid unit not `c` or `f`: %s", unit),
		}
		bytes, err := json.Marshal(resp)
		if err != nil {
			log.Fatalf("error encoding json: %v", err)
		}
		w.Write(bytes)
		return
	}
	if err != nil {
		resp := &Response{
			Temp:   -9999.0,
			Unit:   "n/a",
			Time:   time.Now().Format("Jan 2 03:04 PM MST 2006"),
			Status: fmt.Sprintf("%v", err),
		}
		bytes, err := json.Marshal(resp)
		if err != nil {
			log.Fatalf("error encoding json: %v", err)
		}

		w.Write(bytes)
		return
	}
	resp := &Response{
		Temp:   temp,
		Unit:   unitString,
		Time:   time.Now().Format("Jan 2 03:04 PM MST 2006"),
		Status: "ok",
	}
	bytes, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("error encoding json: %v", err)
	}
	w.Write(bytes)
}

